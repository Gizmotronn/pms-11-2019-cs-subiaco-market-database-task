





COMPUTER SCIENCE

ATAR Course
Year 11 2019

Preliminary TASK 4: 
Data Management and ERD 













Assessment Task for Year 11 ATAR Course
TYPE: 	Project
CONTENT: 	Database Modelling
UNIT LEARNING CONTEXT: Small business
Task 4: System development (60 marks) (5%)    

The Subiaco Independent Supermarket (the SIS), located in Subiaco WA, is a family owned and run business. The business has been in the hands of Roger and Yvonne Pham for about 15 years. They currently have standalone computers, which their children, who have now finished studying, have suggested that they consider updating. As the Phams have no experience in updating the technology in their business they decide to hire a systems analyst to give them some direction.
The systems analyst, having done a preliminary study, has produced the following analysis of the business.
The supermarket is split into a number of departments such as fruit and veg, butcher, bakery, frozen foods, toiletries etc.  Each department sells an assortment of goods but all goods in the department are of the same type. For example the deli sells olives, cheese, salads and so on, whereas the butcher sells cuts of pork, beef and lamb. Each item for sale is sourced from only one supplier and the supplier can supply many different items for the supermarket to sell.  Each department has a manager who is responsible for all reordering of stock. Managers over time work across all departments. 
Use the study case and complete the requirements below

Time allocation
Two weeks
What you need to do
	
1.	Using the Case Study create an ERD which models the relationship between the departments, the goods and the suppliers. Show all entities, relationships, cardinalities, primary and foreign keys. Create one with M:N relationships, and then separately Resolve any and all M:N relationships.										(16 marks)

2.	Using the Case Study create a second ERD which will model the relationship between the departments and the managers. Show all entities, relationships, cardinalities, primary and foreign keys. Show both before and after all M:N relationships are resolved. 			(16 marks)
				
3.	Create a management relational database.  The database should include tables with data entry validation, forms, queries and reports. Use the data in Appendix A to create the records for the database. Present the results of the QBE (Query By Example) outlined in Appendix B, using appropriately formatted reports.



 
Marking Key—Year 11 ATAR Computer Science
Task 4  									Total 60 marks
Item	Description	Possible mark	Allocated mark
ERD 1 (14 marks)
	Each entity (to a max 3 marks)	1–3	
	Each relationship (to a max 2 marks)	1–2	
	Each correct cardinality set (to a max of 2 marks)	1–2	
	Each primary key (to a max of 3 marks)	1–3	
	each foreign key (to a max of 2 marks)	1–2	
	Other non-key attribute (to a max of 1 mark per entity)	1	
	Correctly drawn logical ERD	1	
  ERD 2 (14 marks)	(NOTE: no marks for Department other than new cardinality)
Each new entity (to a max 2 marks)	1–2	
	Each relationship (to a max 2 marks)	1–2	
	Each correct cardinality set (to a max of 2 marks)	1–2	
	Each primary key (to a max of 1 mark)	1–3	
	Each foreign key (to a max of 2 marks)	1–2	
	Other non-key attribute (to a max of 1 mark per entity)	1	
	Correctly resolved M:N	1	
	Correctly drawn logical ERD	1	
Database application
(26 marks)

	Tables
tables correctly created (to a max 3 marks)
Forms
forms ( 1  mark per form to a max 3 marks)
Queries
queries correctly created (1 – 10 marks) 
Report
reports correctly created (1 - 10 marks)
Navigation System
Create interface, navigation system (1 - 3 marks)
Detailed and fully operational visual interface, navigation system (4 - 6 marks) 	3
3
10
10
6	
		Total	60 / 
Teacher comments




